using System;
using System.Collections.Generic;
using System.Text;

namespace laba.NET
{
    public class Listener
    {

        private System.Collections.Generic.List<ListEntry> _listChanges;

        public Listener()
        {
            _listChanges = new List<ListEntry>();
        }
        
        
        /// <summary>
        /// обробник подій який створює елемент ListEntry і додає його в список змін
        /// </summary>
        /// <param name="source"></param>
        /// <param name="args"></param>
        public void MagazineAdded(object source, MagazineListHandlerEventArgs args)
        {
           ListEntry ls = new ListEntry(args.NumberElementChanges, args.TypeChangeCollections, args.NameCollectionsEvent);
           _listChanges.Add(ls);
        }

        public void MagazineAReplace(object source, MagazineListHandlerEventArgs args)
        {
            ListEntry ls = new ListEntry(args.NumberElementChanges, args.TypeChangeCollections, args.NameCollectionsEvent);
            _listChanges.Add(ls);
        }
        
        /// <summary>
         /// формування рядка з інформацією про всі елементи List<ListEntry>
         /// </summary>
         /// <returns></returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
           
            sb.Append("All List Element:");
            foreach (var a in _listChanges)
            {
                sb.Append(a.ToString());
                sb.Append("\n");
            }

            return sb.ToString();
        }
    }
}