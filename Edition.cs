﻿using System;
using System.Text;

namespace laba.NET
{
    [Serializable]
    public class Edition : System.IComparable, System.Collections.Generic.IComparer<Edition>
    {
        /// <summary>
        /// Назва публiкації
        /// </summary>
        protected string _name;

        /// <summary>
        /// дата публікації
        /// </summary>
        protected DateTime _dateRelease;

        /// <summary>
        /// тираж
        /// </summary>
        protected int _countPublication;


        public Edition(string name, DateTime dateRelease, int countPublication)
        {
            Name = name;
            DateRelease = dateRelease;
            CountPublication = countPublication;
        }

        public Edition() : this("DeutschBC", DateTime.Now, 0)

        {
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public virtual DateTime DateRelease
        {
            get { return _dateRelease; }
            set { _dateRelease = value; }
        }

        public virtual int CountPublication
        {
            get { return _countPublication; }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException("Тираж не може бути ві'ємним числом");
                }

                _countPublication = value;
            }
        }

        public Edition ShallowCopy()
        {
            return (Edition)this.MemberwiseClone();
        }
        
        public virtual object DeepCopy()
        {
            Edition e = new Edition(Name, DateRelease, CountPublication);
            return e;
        }
        

        public override bool Equals(Object obj)
        {
            if (obj == null || !GetType().Equals(obj.GetType()))
            {
                return false;
            }

            Edition e = (Edition) obj;
            return Name == e.Name && DateRelease == e.DateRelease && CountPublication == e.CountPublication;
        }

        public static bool operator ==(Edition obj1, Edition obj2)
        {
            return !ReferenceEquals(obj1, null) && obj1.Equals(obj2);
        }

        public static bool operator !=(Edition obj1, Edition obj2)
        {
            return !ReferenceEquals(obj1, null) && !obj1.Equals(obj2);
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }

        public override string ToString()
        {
            return 
                $"name_publication {Name} {Environment.NewLine}date_release {DateRelease} {Environment.NewLine}count_publication {CountPublication} {Environment.NewLine}";
       
        }

        //інтерфейс System.IComparable для порівняння об,єктів
        //Edition по полю з назвою видання
        public int CompareTo(object obj)
        {
            if (obj == null) return 1;
        
            Edition otherName = obj as Edition;
            if (otherName != null)
                return this._name.CompareTo(otherName._name);
            else return -1;
        }
        
        //інтерфейс System.Collections.Generic.IComparer<Edition>
        //для порівняння об,єктів по даті виходу видання
        public int Compare(Edition x, Edition y)
        {
            return x._dateRelease.CompareTo(y._dateRelease);
        }
    }
}