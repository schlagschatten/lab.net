using System;

namespace laba.NET
{
    public class ListEntry
    {
        private int _numberAddChangesElement;
        
        public string NameCollectionsEvent{ 
            get;
            set;
        }
        
        public string InformationEvent{ 
            get;
            set;
        }
        
        public ListEntry(int numberAddChangesElement, string nameCollectionsEvent, string informationEvent)
        {
            _numberAddChangesElement = numberAddChangesElement;
            NameCollectionsEvent = nameCollectionsEvent;
            InformationEvent = informationEvent;
        }
        
        public override string ToString()
        {
            return 
                $"_numberAddChangesElement {_numberAddChangesElement} {Environment.NewLine}NameCollectionsEvent {NameCollectionsEvent} {Environment.NewLine}InformationEvent {InformationEvent} {Environment.NewLine}";
        }
        
    }
}