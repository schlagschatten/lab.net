﻿using System;

namespace laba.NET
{
    public interface IRateAndCopy
    {
        double Rating { get; }
        
        
        object DeepCopy();
    }
}